<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>upload tasks</title>

<link href="pm/css/style.css" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="bootstrap/css/layout.css" type="text/css"
	media="screen" />
<link rel="stylesheet" href="pm/css/jquery-ui.css">
	<script src="pm/js/jquery.min.js"></script>

<script src="pm/js/jquery-1.11.2.min.js"></script>
<script src="pm/js/jquery-ui.js"></script>
<script>	
		$(function() {
		$("#datepicker").datepicker({
			dateFormat : 'yy-mm-dd'
		});

		$("#taskDomain").selectmenu();

		$("#files").selectmenu();

		$("#number").selectmenu().selectmenu("menuWidget").addClass("overflow");

		var dialog;

		dialog = $("#dialog-form").dialog({
			autoOpen : false,
			height : 300,
			width : 350,
			modal : true,
			buttons : {
				"Create an account" : addTask,
				Cancel : function() {
					this.dialog("close");
				}
			},
			close : function() {
				allFields.removeClass("ui-state-error");
			}
		});
	});
	$('body').on('click', '#create-task', function() {
		dialog.dialog("open");
	});
</script>
<style>
fieldset {
	border: 0;
	width: 200px;
}

label {
	display: block;
	margin: 30px 0 0 0;
}

select {
	width: 200px;
}

.overflow {
	height: 200px;
}

table th:first-child {
	width: 150px;
}
/* Bootstrap 3.x re-reset */
.fn-gantt *,.fn-gantt *:after,.fn-gantt *:before {
	-webkit-box-sizing: content-box;
	-moz-box-sizing: content-box;
	box-sizing: content-box;
}
</style>
</head>
<body>

	<div class="contain">
		<div class="gantt"></div>
	</div>


	<%
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
	%>
	<section id="main" class="column"> <article
		class="module width_full"> <header>
	<h3>Tasks of the Project</h3>
	</header>
	<div class="module_content">
		<article class="stats_graph">
		<div class="form-group">
			<s:form action="updateProj">
				<s:textfield label="Project Name" value="%{idea.ideaTitle}"
					name="ideaTitle"></s:textfield>
				<s:textfield label="Project Cost" name="ideaBudget"
					value="%{idea.ideaBudget}"></s:textfield>
				<s:textfield label="Project" name="ideaDuration"
					value="%{idea.ideaDuration}"></s:textfield>
				<s:textfield label="Project" name="ideaDescrption"
					value="%{idea.ideaDescrption}"></s:textfield>
				<%-- 						<s:submit value="Update" ></s:submit> --%>
<%-- 				<input type="hidden" class="form-control"
					value="<%=session.getAttribute("userid")%>" name="proOwner" />
				<s:hidden name="proID" value="%{idea.ideaId}"></s:hidden> --%>

			</s:form>
			<!-- 		<button type="button" id="create-task">Create new task</button> -->

			<%-- <s:form id="frmTask" action="deleteProject">
			<s:submit value="Delete" ></s:submit>
			<s:hidden name="proID" value="%{proj.proID}"></s:hidden>			
<input type="hidden" class="form-control" value="<%=session.getAttribute("userid")%>" name="proOwner" /> 
			</s:form> --%>



		</div>
		<div class="module-body"></div>
		</article>

		<div class="tab_container">
			<div id="tab1" class="tab_content">
				<table class="tablesorter" cellspacing="0">
					<thead>
						<tr>
							<th align="left">Title</th>
							<th>Description</th>
							<th>Domain</th>
							<th>Posted on</th>
							<th>Budget</th>
							<th>Duration</th>
						</tr>
					</thead>
					<s:iterator value="tasks">
						<tr>
							<td><s:property value="taskTitle" /></td>
							<td><s:property value="taskDescrption" /></td>
							<td><s:property value="taskDomain" /></td>
							<td><s:property value="taskPostedOn" /></td>
							<td><s:property value="taskBudget" /></td>
							<td><s:property value="taskDuration" /></td>
							<td><s:form action="projectTask">
							<s:hidden name="taskID" value="%{taskID}"></s:hidden>									
									<s:submit value="Open"></s:submit>
								</s:form></td>
						</tr>
					</s:iterator>
				</table>
			</div>

		</div>

		<div class="clear"></div>
	</div>
	<div class="module_content">
		<article class="stats_graph">
		<div class="form-group">
			<br>
		</div>
		<div class="module-body">
			<div class="tab_container">
				<div id="tab1" class="tab_content">
					<table class="tablesorter" cellspacing="0">
						<thead>
							<tr>
								<th align="left"> username</th>
								<th align="left">firstname</th>
								<th>lastname</th>
								<th>country</th>
							</tr>
						</thead>
<tr>
	<td>
		<s:property value="%{user.username}"/>
		</td>
			<td>
		<s:property value="%{user.firstname}"/>		
				</td>			
			<td>		
		<s:property value="%{user.lastname}"/>
				</td>		
			<td>		
		<s:property value="%{user.country}"/>	
				</td>		
                   			</tr>	 
                   </table>
         		<div  id="jRate" style="height:10px;width: 40px;"></div>
          
		 <s:form action="rateEn">
 <input type="hidden" class="form-control" value="<%=session.getAttribute("userid")%>" name="rater" />
<s:hidden id="rating" name="rating"></s:hidden>									
<s:hidden name="ideaId" value="%{idea.ideaId}"></s:hidden>									
<s:submit></s:submit>
      </s:form>
	 <s:form action="p2eMsg">
<s:textarea name="ideaCommit" ></s:textarea>
<s:hidden name="ideaId" value="%{idea.ideaId}"></s:hidden>									
<s:submit></s:submit>
      </s:form>

	<div id="dialog-form">
		<s:form action="ideaTaskPost">
			<label> Task Title </label>
			<input type="text" class="form-control" name="taskTitle"
				placeholder="Enter Project Title">
			<input type="hidden" class="form-control" name="taskPostedOn"
				value="${currentDate}" />


			<label> Task Category </label>
			<fieldset>
				<select name="taskDomain" id="taskDomain">
					<option>web</option>
					<option>Android</option>
					<option selected="selected">IOS</option>
					<option>Desktop</option>
					<option>other</option>
				</select>
			</fieldset>

			<label> Project Budget </label>
			<input type="text" class="form-control" name="taskBudget"
				placeholder="Enter Project Budget">
			<label> Task Submission </label>
			<input type="text" id="datepicker" class="form-control"
				placeholder="Enter Project Duration" name="taskDuration" size="30" />


			<label> Task Description </label>
			<textarea name="taskDescrption" class="form-control" rows="3"></textarea>
			<s:hidden name="taskIdeaId" value="%{idea.ideaId}"></s:hidden>
			<%-- 		<s:textfield label="Project Id" name="%{#parameters.taskProId} " ></s:textfield> --%>
			<s:submit value="submit Task"></s:submit>
		</s:form>
	</div>
	</article>
	<div class="clear"></div>
	<div class="spacer"></div>
	</section>
		<script src="pm/js/jquery.min.js"></script>

	<script src="pm/js/jquery.fn.gantt.js"></script>
	<script src="pm/js/jquery-te-1.4.0.min.js"></script>

	<script src="js/jRate.min.js"></script>
	<script type="text/javascript">
	var src = [];

	$(function() {
		
		$("#jRate").jRate({
			rating: 3,
			strokeColor: 'black',
			width: 20,
			height: 20,						
		 	onChange: function(rating) {
				console.log("OnChange: Rating: "+rating);
			}, 
			onSet: function(rating) {
				$('#rating').val(rating);
				//alert("hello baba"+rating);
			}
		});
	});
	</script>
	<s:iterator value="tasks">
		<script>
			$(function() {

				var sdate = new Date("${taskPostedOn}");
				var smilli = sdate.getTime();

				var ldate = new Date("${taskDuration}");
				var lmilli = ldate.getTime();

				src.push({
					name : "${taskTitle}",
					desc : "${taskDescrption}",
					values : [ {
						from : "/Date(" + smilli + ")/",
						to : "/Date(" + lmilli + ")/",
						label : "${taskDomain}",
						customClass : "ganttBlue"
					} ]

				});
			});
		</script>
	</s:iterator>
	<script>
		$(function() {
			"use strict";
			$(".gantt").gantt({
				source : src,
				navigate : "scroll",
				maxScale : "hours",
				itemsPerPage : 10
			});
			prettyPrint();

		});
	</script>
</body>
</html>