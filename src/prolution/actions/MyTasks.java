package prolution.actions;

import java.util.ArrayList;

import prolution.dao.taskDao;
import prolution.entity.tb_idea;
import prolution.entity.tb_tasks;

public class MyTasks {
	
	
	private ArrayList<tb_tasks> tasks = new ArrayList<tb_tasks>();
	private int taskOwner;
	
	taskDao daoTask=new taskDao();
	
	public String execute(){
		
	this.tasks=daoTask.myTasks(getTaskOwner());
	return "success";
	}
	public ArrayList<tb_tasks> getTasks() {
		return tasks;
	}
	public void setTasks(ArrayList<tb_tasks> tasks) {
		this.tasks = tasks;
	}
	public int getTaskOwner() {
		return taskOwner;
	}
	public void setTaskOwner(int taskOwner) {
		this.taskOwner = taskOwner;
	}
	
	

}
