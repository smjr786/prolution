package prolution.actions;



import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;







import prolution.dao.UserDao;





public class LoginAction extends ActionSupport implements SessionAware {
	
	private static final long serialVersionUID = 1L;
	private Map<String, Object> sessionMap;
	private String username;
	private int userid;
	private String upassword;
    private double rating;
	private String firstname;

    
    
	
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}


	UserDao dao=new UserDao();
    
    
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
   
   
    public String getUpassword() {
		return upassword;
	}
	public void setUpassword(String upassword) {
		this.upassword = upassword;
	}


	String loggedUsername = null;
	String loggedFirstname = null;
  
    
   
       
    
        @Override
        public void setSession(Map<String, Object> sessionMap) {
            this.sessionMap = sessionMap;
        }  
        
        
       
        
        public String login(){
        if(sessionMap.containsKey("username")) {
            loggedUsername = (String) sessionMap.get("username");
            loggedFirstname = (String) sessionMap.get("firstname");
        }
        
        
    
        if(username == null || username.trim().equals("")){
           this.addFieldError("username", "username is required");
        return "input";}
        if(upassword.length() == 0){
            this.addFieldError("password", "Password is required"); 
        return "input";}
        

   
        if(dao.find(getUsername(),getUpassword())){
        	String str=dao.rtype(getUsername());
        	sessionMap.put("username", username);
        	sessionMap.put("firstname", firstname);
        	sessionMap.put("userid", dao.getUserId(getUsername()));
        	this.rating=dao.userRanking(dao.getUserId(getUsername()));
        	if(str.equals("pm"))
        	return "p";
        	else if(str.equals("cn"))
        		return "c";
        	else	
        	return "e";
        }
        
        else
        {
            this.addActionError("Invalid username and password");
            return "error";}
    
    }
        
        public String logout(){
        	if (sessionMap.containsKey("username")) {
                sessionMap.remove("username");
            }
            return "SUCCESS";
        }
    
    
    
    
 
 
 

}
