<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Pro-Lution</title>
<link rel="stylesheet" href="bootstrap/css/layout.css" type="text/css" media="screen" />
<link href="css/gstyle.css" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="pm/css/jquery-ui.css">

<script src="pm/js/jquery.min.js"></script>
<script src="pm/js/jquery-1.11.2.min.js"></script>
<script src="pm/js/jquery-ui.js"></script>
<script>
	$(function() {
		$("#datepicker").datepicker({
			dateFormat : 'yy-mm-dd'
		});

		$("#proDomain").selectmenu();

		$("#files").selectmenu();

		$("#number").selectmenu().selectmenu("menuWidget").addClass("overflow");
	});
</script>
<style>
fieldset {
	border: 0;
	width: 200px;
}

label {
	display: block;
	margin: 30px 0 0 0;
}

select {
	width: 200px;
}

.overflow {
	height: 200px;
}

table th:first-child {
	width: 150px;
}
/* Bootstrap 3.x re-reset */
.fn-gantt *,.fn-gantt *:after,.fn-gantt *:before {
	-webkit-box-sizing: content-box;
	-moz-box-sizing: content-box;
	box-sizing: content-box;
}
</style>
</head>
<body>
	<%
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
	%>
	<section id="main" class="column"> <article
		class="module width_full"> <header>
	<h3>List of Current Projects</h3>
	</header>
	<div class="module_content">
		<article class="stats_graph">
		<div class="module-body">
			<div class="tab_container">
				<div id="tab1" class="tab_content">
					<table class="tablesorter" cellspacing="0">
						<thead>
							<tr>
								<th align="left">Project Name</th>
								<th align="left">Project Description</th>
								<th>Domain</th>
								<th>Posted on</th>
								<th>Budget</th>
								<th>Duration</th>
							</tr>
						</thead>
						<s:iterator value="project">
							<%-- <s:url id="update" action="myProject">
								<s:param name="proj.proID" value="proID" />
								<s:param name="taskProId" value="proID" />
							</s:url> --%>
							<tr>
								<td><s:property value="proTitle" /></td>
								<td><s:property value="proDescrption" /></td>
								<td><s:property value="proDomain" /></td>
								<td><s:property value="proPostedOn" /></td>
								<td><s:property value="proBudget" /></td>
								<td><s:property value="proDuration" /></td>
								<td><s:form action="projectTasks">
										<s:hidden name="proID" value="%{proID}"></s:hidden>
										<s:submit value="Open"></s:submit>
									</s:form></td>
							</tr>
						</s:iterator>
						<s:iterator value="ideas">
							<%-- <s:url id="update" action="myProject">
								<s:param name="proj.proID" value="proID" />
								<s:param name="taskProId" value="proID" />
							</s:url> --%>
							<tr>
								<td><s:property value="ideaTitle" /></td>
								<td><s:property value="ideaDescrption" /></td>
								<td><s:property value="ideaDomain" /></td>
								<td><s:property value="ideaPostedOn" /></td>
								<td><s:property value="ideaBudget" /></td>
								<td><s:property value="ideaDuration" /></td>
								<td><s:form action="ideaTasks">
										<s:hidden name="ideaId" value="%{ideaId}"></s:hidden>
										<s:submit value="Open"></s:submit>
									</s:form></td>
							</tr>
						</s:iterator>
					</table>
				</div>
			</div>
		</div>
		
<article
		class="module width_full"> <header>
	<h3>Upload New Project</h3>
	</header>
	</article>

		<div class="module-body">

			<s:form action="projectposted">
				<div class="flot-chart">
					<div class="form-group">
						<label> Task Title </label>
						<%-- 				<s:hidden name="proOwner" value="%{sessionScope.userid}"></s:hidden> --%>

						<input type="hidden" class="form-control"
							value="<%=session.getAttribute("userid")%>" name="proOwner" /> <input
							type="text" class="form-control" name="proTitle"
							placeholder="Enter Project Title"> 
							<input type="hidden"
							class="form-control" name="proPostedOn" value="${currentDate}" />
						<label> Task Category </label>
						<fieldset>
							<select name="proDomain" id="proDomain">
								<option>web</option>
								<option>Android</option>
								<option selected="selected">IOS</option>
								<option>Desktop</option>
								<option>other</option>
							</select>
						</fieldset>
						<label> Task Budget </label> 
						<input type="text"
							class="form-control" name="proBudget"
							placeholder="Enter Project Budget"> <label>
							Project Submission </label> <input type="text" id="datepicker"
							class="form-control" placeholder="Enter Project Duration"
							name="proDuration" size="30"> <label> Project
							Description </label>
						<textarea name="proDescrption" class="form-control" rows="3"></textarea>
					</div>
					<div class="module-body">
						<p>
					</div>
				</div>
				<s:submit></s:submit>
			</s:form>
		</div>
		</article>
		<div class="clear"></div>
	</div>
	</article><!-- end of stats article -->
	<div class="clear"></div>
	<div class="spacer"></div>
	
		</div>

		
		</article>
	</section>

</body>
</html>