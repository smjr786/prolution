package prolution.actions;


import prolution.dao.taskDao;

import com.opensymphony.xwork2.ActionSupport;

public class Tpost extends ActionSupport{
	
	private static final long serialVersionUID = 1L;

private int taskOwner;
	
	private int taskProId;
	
	private String taskTitle;
	
	private String taskDomain;
	
	private String taskPostedOn;
	
	private String taskBudget;
	
	private String taskDuration;
	
	private int taskFlag;
		
	private String taskDescrption;



taskDao tskDao=new taskDao();



	public int getTaskFlag() {
	return taskFlag;
}

public void setTaskFlag(int taskFlag) {
	this.taskFlag = taskFlag;
}

	public int getTaskOwner() {
	return taskOwner;
}

public void setTaskOwner(int taskOwner) {
	this.taskOwner = taskOwner;
}

public int getTaskProId() {
	return taskProId;
}

public void setTaskProId(int taskProId) {
	this.taskProId = taskProId;
}

public String getTaskTitle() {
	return taskTitle;
}

public void setTaskTitle(String taskTitle) {
	this.taskTitle = taskTitle;
}

public String getTaskDomain() {
	return taskDomain;
}

public void setTaskDomain(String taskDomain) {
	this.taskDomain = taskDomain;
}

public String getTaskPostedOn() {
	return taskPostedOn;
}

public void setTaskPostedOn(String taskPostedOn) {
	this.taskPostedOn = taskPostedOn;
}

public String getTaskBudget() {
	return taskBudget;
}

public void setTaskBudget(String taskBudget) {
	this.taskBudget = taskBudget;
}

public String getTaskDuration() {
	return taskDuration;
}

public void setTaskDuration(String taskDuration) {
	this.taskDuration = taskDuration;
}


public String getTaskDescrption() {
	return taskDescrption;
}

public void setTaskDescrption(String taskDescrption) {
	this.taskDescrption = taskDescrption;
}

public static long getSerialversionuid() {
	return serialVersionUID;
}

	public String execute(){
		tskDao.post( taskProId, taskOwner, taskTitle,  taskDescrption,  taskDomain,  taskPostedOn,
				 taskBudget, taskDuration, taskFlag);
		return "success";
	}

}
