<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="/struts-tags" prefix="s" %>
    
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Login Form</title>
  <link rel="stylesheet" href="bootstrap/css/signin.css">
</head>


<body onload="window.history.forward(1);">
<%
   response.setHeader( "Pragma", "no-cache" );
   response.setHeader( "Cache-Control", "no-cache" );
   response.setDateHeader( "Expires", 0 );
%>

  	<div class="incontainer">
    	<div class="login">
      		<h3 align="left"><strong>L o g i n </strong></h3>
      		
      		<s:form method="post" action="login">
				<s:actionerror />
        		<p><s:textfield name="username" required="true" label="Username" placeholder="Username or Email"></s:textfield></p>
        		<p><s:password name="upassword" required="true" label="Password" ></s:password></p>        
        		<p class="submit"><s:submit value="Login" ></s:submit> </p>
      		</s:form>
			
			<s:form method="post" action="rememberusername">
      			<p class="remember_me">
          				<label>
            			<input type="checkbox" name="remember_me" id="remember_me">
						Remember me on this computer
          				</label>
          				</p>
			</s:form>        		
		</div>

    	<div class="login-help">
      		<p>Forgot your password? <a href="home.jsp">Click here to reset it</a>.</p>
    	</div>
  	
  	
  	</div>


</body>
</html>