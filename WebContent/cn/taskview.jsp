<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="utf-8"/>
	<title>Dashboard I Admin Panel</title>
	
	<link rel="stylesheet" href="project/css/layout.css" type="text/css" media="screen" />
	</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title"><img src="project/images/ProLogo.png" alt="logo1" height="60" width="180"></h1>
			<h2 class="section_title">Dashboard</h2><div class="btn_view_site"><a href="">Logout</a></div>
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
			<p>Mirza</p>
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="dashboard.jsp">Contributor</a> <div class="breadcrumb_divider"></div> <a class="current">Dashboard</a></article>
		</div>
	</section><!-- end of secondary bar -->
	
	<aside id="sidebar" class="column">
		<form class="quick_search">
			<input type="text" value="Quick Search" onFocus="if(!this._haschanged){this.value=''};this._haschanged=true;">
		</form>
		<hr/>
		<h3>PROFILE</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="#">My Profile</a></li>
			<li class="icn_edit_article"><a href="updateprofile.jsp">Update Profile</a></li>
			<li class="icn_categories"><a href="ratings.jsp">Profile Ratings</a></li>
			</ul>
		<h3>TASKS</h3>
		<ul class="toggle">
			<li class="icn_add_user"><a href="taskview.jsp">View Tasks</a></li>
			<li class="icn_view_users"><a href="completedtasks.jsp">Completed Tasks</a></li>
			<li class="icn_view_users"><a href="assignedtasks.jsp">Assigned Tasks</a></li>
			</ul>
		<h3>bids</h3>
		<ul class="toggle">
			<li class="icn_folder"><a href="acceptedbids.jsp">Accepted Bids</a></li>
			<li class="icn_photo"><a href="rejectedbids.jsp">Rejected Bids</a></li>
		
		</ul>
		
			
		</ul>
		
		<footer>
			<hr />
			<h4><strong>Copyrights@ prolutions.com</strong></h4>
			<p></p>
		</footer>
	</aside><!-- end of sidebar -->
	
	<section id="main" class="column">
		
		<!-- end of stats article -->
		
		<article class="module width_3_quarter">
		<header>
		<h3 class="tabs_involved">TASK VIEW&nbsp;</h3>
		</header>

		<div class="tab_container">
			<div id="tab1" class="tab_content">
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				<tr> 
   					<th></th> 
    				<th>Task Name</th> 
    				<th>Description</th> 
    				<th>Category</th> 
    				<th>Uploaded on</th> 
                    <th>Starting Bid</th>
                    <th>Your Bid</th> 
				</tr> 
			</thead> 
            
			
				<tr> 
   					<td><input type="checkbox"></td> 
    				<td></td> 
    				<td></td> 
    				<td></td> 
    				<td></td>
    				<td></td>
    				<td></td> 
				</tr> 
			
			<tbody> 
				<tr> 
   					<td><input type="checkbox"></td> 
    				<td></td> 
    				<td></td> 
    				<td></td> 
    				<td></td>
    				<td></td>
    				<td></td> 
				</tr> 
				<tr> 
   					<td><input type="checkbox"></td> 
    				<td>&nbsp;</td> 
    				<td>&nbsp;</td> 
    				<td>&nbsp;</td> 
   				 	<td>&nbsp;</td>
   				 	<td></td>
   				 	<td></td> 
				</tr>
				<tr> 
   					<td><input type="checkbox"></td> 
    				<td>&nbsp;</td> 
    				<td>&nbsp;</td> 
    				<td>&nbsp;</td> 
    				<td>&nbsp;</td>
    				<td></td>
    				<td></td> 
				</tr> 
				<tr> 
   					<td><input type="checkbox"></td> 
    				<td>&nbsp;</td> 
    				<td>&nbsp;</td> 
    				<td>&nbsp;</td> 
   				 	<td>&nbsp;</td>
   				 	<td></td>
   				 	<td></td> 
				</tr>  
			</tbody> 
			</table>

			</div><!-- end of #tab2 -->
			 <div class="col-md-8">
              <input type="button" class="btn btn-primary" value="Select Task">
              </div>
		</div><!-- end of .tab_container -->
		
		</article><!-- end of content manager article -->
		
		
			
</form>
			</footer>
		</article><!-- end of messages article -->
		
<div class="clear"></div><!-- end of post new article -->
		
<!-- end of styles article -->
<div class="spacer"></div>
	</section>



</body>
</html>