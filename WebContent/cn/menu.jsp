<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <%@ taglib uri="/struts-tags" prefix="s" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
	<link rel="stylesheet" href="bootstrap/css/layout.css" type="text/css" media="screen" />
</head>
<body>
<%
   response.setHeader( "Pragma", "no-cache" );
   response.setHeader( "Cache-Control", "no-cache" );
   response.setDateHeader( "Expires", 0 );
%>
		<form class="quick_search">
			<input type="text" value="Quick Search" onFocus="if(!this._haschanged){this.value=''};this._haschanged=true;">
		</form>
		<hr/>
		<h3>Profile</h3>
		<ul class="toggle">		
				
			<li class="icn_new_article"><a href="profilePm.jsp">My Profile</a></li>
			<li class="icn_edit_article"><a href="updateProfilePm.jsp">Update Profile</a></li>
			<li class="icn_categories"><a href="#">Profile Ratings</a></li>
			</ul>
		<h3>Projects</h3>
		<ul class="toggle">
		   <li class="icn_new_article">
<s:form action="myTasks" > <input type="hidden" class="form-control" value="<%=session.getAttribute("userid")%>" name="taskOwner" /> 
 <s:submit>my Projects</s:submit> </s:form> </li> 
<li class="icn_edit_article"><s:form action="allTasks" > <input type="hidden" class="form-control" value="<%=session.getAttribute("userid")%>" name="bidsUser" /> 
 <s:submit>all Projects</s:submit> </s:form> </li>
 </ul>     						
<h3>Task</h3>
		<ul class="toggle">
			<li class="icn_add_user"><a href="#">New Requests</a></li>
			<li class="icn_view_users"><a href="#">In progress</a></li>
			<li class="icn_view_users"><a href="#">Accepted</a></li>
			</ul>
		<h3>bids</h3>
		<ul class="toggle">
			<li class="icn_folder"><a href="#">Accepted Bids</a></li>
			<li class="icn_photo"><a href="#">Rejected Bids</a></li>
			<li class="icn_audio"><a href="#">New Bids</a></li>
			
		</ul>
		<h3>scheduling</h3>
		<ul class="toggle">
			<li class="icn_settings"><a href="#">Gantt Chart</a></li>
			
		</ul>
		<h3>Account</h3>
		<ul class="toggle">
			<li class="icn_settings"><a href="#">Pro-Coins</a></li>
			
		</ul>
		<ul class="toggle">
			<li class="icn_settings"><a href="#">Scoring</a></li>
		</ul>
		
		<h3><s:form action="logout"><a href="">Log Out</a></s:form></h3>

</body>
</html>