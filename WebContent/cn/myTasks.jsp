<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
                        <%@taglib uri="/struts-tags" prefix="s"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div class="module_content">
		<article class="stats_graph">
		<div class="module-body">
			<div class="tab_container">
				<div id="tab1" class="tab_content">
					<table class="tablesorter" cellspacing="0">
						<thead>
							<tr>
								<th align="left">Project Name</th>
								<th align="left">Project Description</th>
								<th>Domain</th>
								<th>Posted on</th>
								<th>Budget</th>
								<th>Duration</th>
							</tr>
						</thead>
<s:iterator value="tasks">
	
	<tr>
	<td>
		<s:property value="taskTitle"/>
		</td>
			<td>
		
		<s:property value="taskBudget"/>		
					</td>			
			<td>		
		<s:property value="taskDuration"/>
				</td>		
			<td>		
		<s:property value="taskDomain"/>	
				</td>		
			<td>		
		<s:property value="taskPostedOn"/> 	
				</td>		
			<td>		
		<s:property value="taskDescrption"/> 
			</td>
			<td><s:form action="myTask">
										<s:hidden name="taskID" value="%{taskID}"></s:hidden>
										<s:submit value="Open"></s:submit>
									</s:form></td>	
			</tr>	 
</s:iterator>
</table>
				</div>
			</div>
		</div>
</body>
</html>