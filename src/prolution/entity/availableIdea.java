package prolution.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "availableIdea", catalog = "db_prolution")
public class availableIdea implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8463333885724230502L;



	@Id
	@Column(name = "ideaId", nullable = true)
	private int ideaId;
	


	private String ideaTitle;
	
	private String ideaDomain;
	
	private String ideaPostedOn;
	
	private String ideaBudget;
	
	private String ideaDuration;

	
	private String ideaDescrption;









	public int getIdeaId() {
		return ideaId;
	}


	public void setIdeaId(int ideaId) {
		this.ideaId = ideaId;
	}


	public String getIdeaTitle() {
		return ideaTitle;
	}


	public void setIdeaTitle(String ideaTitle) {
		this.ideaTitle = ideaTitle;
	}


	public String getIdeaDomain() {
		return ideaDomain;
	}


	public void setIdeaDomain(String ideaDomain) {
		this.ideaDomain = ideaDomain;
	}


	public String getIdeaPostedOn() {
		return ideaPostedOn;
	}


	public void setIdeaPostedOn(String ideaPostedOn) {
		this.ideaPostedOn = ideaPostedOn;
	}


	public String getIdeaBudget() {
		return ideaBudget;
	}


	public void setIdeaBudget(String ideaBudget) {
		this.ideaBudget = ideaBudget;
	}


	public String getIdeaDuration() {
		return ideaDuration;
	}


	public void setIdeaDuration(String ideaDuration) {
		this.ideaDuration = ideaDuration;
	}



	public String getIdeaDescrption() {
		return ideaDescrption;
	}


	public void setIdeaDescrption(String ideaDescrption) {
		this.ideaDescrption = ideaDescrption;
	}


	public availableIdea(int ideaID, int ideaOwner, String ideaTitle,
			String ideaDomain, String ideaPostedOn, String ideaBudget,
			String ideaDuration, boolean ideaAproved, boolean ideaSigned,
			boolean ideaCompletion, String ideaDescrption) {
		super();
		this.ideaTitle = ideaTitle;
		this.ideaDomain = ideaDomain;
		this.ideaPostedOn = ideaPostedOn;
		this.ideaBudget = ideaBudget;
		this.ideaDuration = ideaDuration;
		this.ideaDescrption = ideaDescrption;
	}


	public availableIdea() {
		// TODO Auto-generated constructor stub
	}
	
	

	

}
