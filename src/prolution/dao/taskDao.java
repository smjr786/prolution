package prolution.dao;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import prolution.entity.SearchedTasks;
import prolution.entity.TaskAllBids;
import prolution.entity.availabletasks;
import prolution.entity.ideaallbids;
import prolution.entity.tb_bids;
import prolution.entity.tb_idea;
import prolution.entity.tb_project;
import prolution.entity.tb_tasks;

public class taskDao {
	
	public void post(int taskProId,int taskOwner,String taskTitle, String taskDescrption, String taskDomain, String taskPostedOn,
			String taskBudget,String taskDuration,int taskFlag ) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
       
        tb_tasks tsk=new tb_tasks();
        tsk.setTaskFlag(taskFlag);
        tsk.setTaskBudget(taskBudget);
        
        tsk.setTaskDescrption(taskDescrption);
        tsk.setTaskDomain(taskDomain);
        tsk.setTaskDuration(taskDuration);
        tsk.setTaskOwner(taskOwner);
        tsk.setTaskPostedOn(taskPostedOn);
        tsk.setTaskProId(taskProId);
        
        tsk.setTaskTitle(taskTitle);
        
        session.save(tsk);
        session.getTransaction().commit();
        session.close();
    }
	
    @SuppressWarnings("unchecked")
    public ArrayList<availabletasks> allTasks(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from availabletasks t WHERE t.taskID NOT IN (SELECT b.bidsTask from tb_bids b where b.bidsUser='" +id+"')";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        ArrayList<availabletasks> objcoll=new ArrayList<>();
        List<availabletasks> list = query.list();
        for (int i =0; i<list.size(); i++) {
        	availabletasks task=new availabletasks();
        	
        	task.setTaskBudget(list.get(i).getTaskBudget());
        	task.setTaskDescrption(list.get(i).getTaskDescrption());
        	task.setTaskDomain(list.get(i).getTaskDomain());
        	task.setTaskDuration(list.get(i).getTaskDuration());
        	task.setTaskPostedOn(list.get(i).getTaskPostedOn());
        	task.setTaskTitle(list.get(i).getTaskTitle());
        	task.setTaskID(list.get(i).getTaskID());
        
        	objcoll.add(task);
        }       
        session.close();
        return objcoll;

    }
    
    public  boolean bidsTask(int  bidsUser,int bidsTask,int  bidsFlag)
    {
    	 Session session = HibernateUtil.getSessionFactory().openSession();
         session.beginTransaction();
          
         tb_bids bid=new tb_bids();
         bid.setBidsTask(bidsTask);
         bid.setBidsFlag(bidsFlag);
         bid.setBidsUser(bidsUser);
         
         session.save(bid);
         session.getTransaction().commit();
         session.close();
         return true;
    }
    
    @SuppressWarnings("unchecked")
    public ArrayList<tb_tasks> myTasks(int taskOwner) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from tb_tasks t where t.taskOwner='" + taskOwner + "'";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        //session.get(getClass(), arg1)
        //Iterator<project> it = query.iterate();      
        ArrayList<tb_tasks> objcoll=new ArrayList<tb_tasks>();
        List<tb_tasks> list = query.list();
        for (int i =0; i<list.size(); i++) {
        	tb_tasks task=new tb_tasks();
            
            task.setTaskBudget(list.get(i).getTaskBudget());
            task.setTaskDescrption(list.get(i).getTaskDescrption());
            task.setTaskDomain(list.get(i).getTaskDomain());
            task.setTaskDuration(list.get(i).getTaskDuration());
            task.setTaskID(list.get(i).getTaskID());
            task.setTaskProId(list.get(i).getTaskProId());
            task.setTaskOwner(list.get(i).getTaskOwner());
            task.setTaskPostedOn(list.get(i).getTaskPostedOn());
            task.setTaskTitle(list.get(i).getTaskTitle());
            
        	objcoll.add(task);
        }                         
        session.close();
        return objcoll;

    }
    

    @Transactional
    public  void commitTask(int  taskId,String taskCommit)
    {
    	 
    	tb_tasks task=new tb_tasks();
    	
    	Session session = HibernateUtil.getSessionFactory().openSession();
    		Transaction tx = null;
    		try {
    		tx = session.beginTransaction();
    		session.beginTransaction();
    		task=(tb_tasks)session.get(tb_tasks.class, taskId);
    	task.setTaskCommit(taskCommit);
    	
    	   session.update(task);
    	   
    	   tx.commit();
    		 } catch (HibernateException e) {
    	    if (tx!=null) tx.rollback();
    	    e.printStackTrace(); 
    	 }finally {
    	    session.close(); 
    	 }

    	}
    
    public tb_tasks projectTask(int id) {
    	tb_tasks task=new tb_tasks(); 
    	try {
    	Session session = HibernateUtil.getSessionFactory().openSession();
    	task=(tb_tasks)session.get(tb_tasks.class, id);
       session.close();
    	 } catch (Exception e) {      

    }
         return task;

}
    
    
    @SuppressWarnings("unchecked")
    public ArrayList<tb_tasks> projectTasks(int proId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from tb_tasks t where t.taskProId='" + proId + "'";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        //session.get(getClass(), arg1)
        //Iterator<project> it = query.iterate();      
        ArrayList<tb_tasks> objcoll=new ArrayList<tb_tasks>();
        List<tb_tasks> list = query.list();
        for (int i =0; i<list.size(); i++) {
        	tb_tasks task=new tb_tasks();
            
            task.setTaskBudget(list.get(i).getTaskBudget());
            task.setTaskDescrption(list.get(i).getTaskDescrption());
            task.setTaskDomain(list.get(i).getTaskDomain());
            task.setTaskDuration(list.get(i).getTaskDuration());
            task.setTaskID(list.get(i).getTaskID());
            task.setTaskProId(list.get(i).getTaskProId());
            task.setTaskOwner(list.get(i).getTaskOwner());
            task.setTaskPostedOn(list.get(i).getTaskPostedOn());
            task.setTaskTitle(list.get(i).getTaskTitle());
            
        	objcoll.add(task);
        }                         
        session.close();
        return objcoll;

    }
    
    @SuppressWarnings("unchecked")
    public ArrayList<tb_tasks> ideaTasks(int ideaId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from tb_tasks t where t.taskIdeaId='" + ideaId + "'";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        //session.get(getClass(), arg1)
        //Iterator<project> it = query.iterate();      
        ArrayList<tb_tasks> objcoll=new ArrayList<tb_tasks>();
        List<tb_tasks> list = query.list();
        for (int i =0; i<list.size(); i++) {
        	tb_tasks task=new tb_tasks();
            
            task.setTaskBudget(list.get(i).getTaskBudget());
            task.setTaskDescrption(list.get(i).getTaskDescrption());
            task.setTaskDomain(list.get(i).getTaskDomain());
            task.setTaskDuration(list.get(i).getTaskDuration());
            task.setTaskID(list.get(i).getTaskID());
            task.setTaskOwner(list.get(i).getTaskOwner());
            task.setTaskPostedOn(list.get(i).getTaskPostedOn());
            task.setTaskTitle(list.get(i).getTaskTitle());
            
        	objcoll.add(task);
        }                         
        session.close();
        return objcoll;

    }
    
    
    

    
    
    @SuppressWarnings("unchecked")
    public ArrayList<SearchedTasks> searchedTasks() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from SearchedTasks";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        ArrayList<SearchedTasks> objcoll=new ArrayList<>();
        List<SearchedTasks> list = query.list();
        for (int i =0; i<list.size(); i++) {
        	SearchedTasks task=new SearchedTasks();
        	
        	task.setNoOfBids(list.get(i).getNoOfBids());
        	//task.setTaskAproved(list.get(i).isTaskAproved());////check the commented values
        	//task.setTaskCompletion(list.get(i).isTaskCompletion());
//        	task.setTaskOwner(list.get(i).getTaskOwner());
        	//task.setTaskSigned(list.get(i).isTaskSigned());
        	task.setTaskBudget(list.get(i).getTaskBudget());
        	task.setTaskDescrption(list.get(i).getTaskDescrption());
        	task.setTaskDomain(list.get(i).getTaskDomain());
        	task.setTaskDuration(list.get(i).getTaskDuration());
        	task.setTaskPostedOn(list.get(i).getTaskPostedOn());
        	task.setTaskTitle(list.get(i).getTaskTitle());
        	task.setTaskID(list.get(i).getTaskID());
        
        	objcoll.add(task);
        }       
        session.close();
        return objcoll;
    }
    
    @SuppressWarnings("unchecked")
    public ArrayList<SearchedTasks> searchedWebTasks() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from SearchedTasks f WHERE f.taskDomain='web'";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        ArrayList<SearchedTasks> objcoll=new ArrayList<>();
        List<SearchedTasks> list = query.list();
        for (int i =0; i<list.size(); i++) {
        	SearchedTasks task=new SearchedTasks();
        	//task.setTaskFlag(list.get(i).getTaskFlag());
        	task.setNoOfBids(list.get(i).getNoOfBids());
        	//task.setTaskAproved(list.get(i).isTaskAproved());  ///check thecommented values
        	task.setTaskCommit(list.get(i).getTaskCommit());
        	//task.setTaskCompletion(list.get(i).isTaskCompletion());
        	task.setTaskOwner(list.get(i).getTaskOwner());
        	//task.setTaskSigned(list.get(i).isTaskSigned());
        	task.setTaskBudget(list.get(i).getTaskBudget());
        	task.setTaskDescrption(list.get(i).getTaskDescrption());
        	task.setTaskDomain(list.get(i).getTaskDomain());
        	task.setTaskDuration(list.get(i).getTaskDuration());
        	task.setTaskPostedOn(list.get(i).getTaskPostedOn());
        	task.setTaskTitle(list.get(i).getTaskTitle());
        	task.setTaskID(list.get(i).getTaskID());
        
        	objcoll.add(task);
        }       
        session.close();
        return objcoll;
    }
    
    @SuppressWarnings({ "unchecked", "finally" })
    public ArrayList<TaskAllBids> taskAllBids(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        ArrayList<TaskAllBids> objcoll=new ArrayList<>();
        String SQL_QUERY = "from TaskAllBids f WHERE (f.taskID='" +id+ "')";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        List<TaskAllBids> list = query.list();
        for (int i =0; i<list.size(); i++) {
        	TaskAllBids taskAllBids=new TaskAllBids();
        	
        	taskAllBids.setCountry(list.get(i).getCountry());
        	taskAllBids.setFirstname(list.get(i).getFirstname());
        	taskAllBids.setLastname(list.get(i).getLastname());
        	taskAllBids.setUsername(list.get(i).getUsername());
        	taskAllBids.setTaskBudget(list.get(i).getTaskBudget());
        	taskAllBids.setTaskDescrption(list.get(i).getTaskDescrption());
        	taskAllBids.setTaskDomain(list.get(i).getTaskDomain());
        	taskAllBids.setTaskDuration(list.get(i).getTaskDuration());
        	taskAllBids.setTaskID(list.get(i).getTaskID());
        	taskAllBids.setTaskPostedOn(list.get(i).getTaskPostedOn());
        	taskAllBids.setTaskTitle(list.get(i).getTaskTitle());
        	taskAllBids.setUserid(list.get(i).getUserid());
        
        	objcoll.add(taskAllBids);
        }
             

        	  session.close(); 
        	  return objcoll; 
        	}        
 
    
    
		  
			public void assignTask(Integer taskID,int taskFlag,int taskOwner) {
			  
			Session session = HibernateUtil.getSessionFactory().openSession();
			Transaction tx = null;
			try {
			tx = session.beginTransaction();
			tb_tasks task=(tb_tasks)session.get(tb_tasks.class, taskID);
			task.setTaskFlag(taskFlag);
			task.setTaskOwner(taskOwner);					
		   
		   session.update(task);
		   
		   tx.commit();
			 } catch (HibernateException e) {
		    if (tx!=null) tx.rollback();
		    e.printStackTrace(); 
		 }finally {
		    session.close(); 
		 }

		}
		  
			public void ideaTaskPost(int taskIdeaId,int taskOwner,String taskTitle, String taskDescrption, String taskDomain, String taskPostedOn,
					String taskBudget,String taskDuration,int taskFlag ) {
		        Session session = HibernateUtil.getSessionFactory().openSession();
		        session.beginTransaction();
		       
		        tb_tasks tsk=new tb_tasks();
		        tsk.setTaskFlag(taskFlag);
		        tsk.setTaskBudget(taskBudget);
		        
		        tsk.setTaskDescrption(taskDescrption);
		        tsk.setTaskDomain(taskDomain);
		        tsk.setTaskDuration(taskDuration);
		        tsk.setTaskOwner(taskOwner);
		        tsk.setTaskPostedOn(taskPostedOn);
		        tsk.setTaskIdeaId(taskIdeaId);
		        tsk.setTaskTitle(taskTitle);
		        
		        session.save(tsk);
		        session.getTransaction().commit();
		        session.close();
		    }

		    public  void p2cMsg(Integer taskID,String taskCommit)
		    {  	
		  	Session session = HibernateUtil.getSessionFactory().openSession();
		  Transaction tx = null;
		 
		  try{
		     tx = session.beginTransaction();
		     tb_tasks task = (tb_tasks)session.get(tb_tasks.class, taskID); 
		     task.setTaskCommit(taskCommit);
		  session.update(task); 
		  tx.commit();
		}catch (HibernateException e) {
		  if (tx!=null) tx.rollback();
		  e.printStackTrace(); 
		}finally {
		  session.close(); 
		}}
}
