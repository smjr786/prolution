<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Available Ideas</title>
	<link rel="stylesheet" href="bootstrap/css/layout.css" type="text/css" media="screen" />

    <link href="assets/plugins/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/plugins/pace/pace-theme-big-counter.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet" />
      <link href="assets/css/main-style.css" rel="stylesheet" />
        <link rel="stylesheet" href="pm/css/jquery-ui.css">
          <link rel="stylesheet" href="pm/css/style.css">
     
<script src="pm/js/jquery-1.11.2.min.js"></script>
  <script src="pm/js/jquery-ui.js"></script>
  <script type="text/javascript">
	$('body').on('click' , '#create-task' , function() { alert("kio"); });

  </script>
</head>
<body>
<section id="main" class="column">
		
		
		
		<article class="module width_full">
			<header>
			  <h3>Available Projects</h3></header>
			  	<div class="module_content">
		<article class="stats_graph">
		<div class="module-body">
			<div class="tab_container">
			<div id="tab1" class="tab_content">
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				<tr> 
   					 
    				<th align="left" >Idea Title </th> 
    				<th>Description</th> 
    				<th>Domain</th> 
    				<th>Posted on</th> 
    				<th>Budget</th>
    				<th>Duration</th>
				</tr> 
			</thead> 
		<s:iterator value="ideas">
			<tr>
				<td><s:property value="ideaTitle" /></td>
				<td><s:property value="ideaDescrption" /></td>				
				<td><s:property value="ideaDomain" /></td>
				<td><s:property value="ideaPostedOn" /></td>
				<td><s:property value="ideaBudget" /></td>
				<td><s:property value="ideaDuration" /></td>
				<td>
				<s:form action="bidsIdea">
					<input type="hidden" value="<%=session.getAttribute("userid")%>"
						name="bidsUser" />
					<s:hidden type="hidden" value="%{ideaId}" name="bidsIdea" />
					<input type="hidden" value="1" name="bidsFlag" />
					
					<s:submit value="Ok"></s:submit>
									</s:form>
					</td>
			</tr>
		</s:iterator>
	</table>
			</div>
			
		</div>
				</div>
				</div>

		
				<div class="clear"></div>
	
		</article>		 
		
	</section>
</body>
</html>