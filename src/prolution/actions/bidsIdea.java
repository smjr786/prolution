package prolution.actions;

import java.util.ArrayList;

import prolution.dao.ideaDao;
import prolution.entity.availableIdea;

public class bidsIdea {
	
	private  int bidsUser;
	private int bidsIdea;
	private int bidsFlag;
	private ArrayList<availableIdea> ideas = new ArrayList<availableIdea>();


	
	




	public int getBidsFlag() {
		return bidsFlag;
	}



	public void setBidsFlag(int bidsFlag) {
		this.bidsFlag = bidsFlag;
	}



	public int getBidsUser() {
		return bidsUser;
	}



	public void setBidsUser(int bidsUser) {
		this.bidsUser = bidsUser;
	}



	public int getBidsIdea() {
		return bidsIdea;
	}



	public void setBidsIdea(int bidsIdea) {
		this.bidsIdea = bidsIdea;
	}



ideaDao IDEAdao=new ideaDao();

	public String execute(){
		IDEAdao. bidsIdea(  getBidsUser(), getBidsIdea(),  getBidsFlag());
		this.ideas=IDEAdao.allIdeas(getBidsUser());
		return "success";
	}



	public ArrayList<availableIdea> getIdeas() {
		return ideas;
	}



	public void setIdeas(ArrayList<availableIdea> ideas) {
		this.ideas = ideas;
	}
	
	
}
