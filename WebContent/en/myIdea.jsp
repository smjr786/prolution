<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
                <%@taglib uri="/struts-tags" prefix="s"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<div class="module_content">
		<article class="stats_graph">
		<div class="form-group">
			<s:form action="updateProj">
				<s:textfield label="Idea Name" value="%{idea.ideaTitle}"
					name="ideaTitle"></s:textfield>
				<s:textfield label="idea Cost" name="ideaBudget"
					value="%{idea.ideaBudget}"></s:textfield>
				<s:textfield label="idea duration" name="ideaDuration"
					value="%{idea.ideaDuration}"></s:textfield>
				<s:textfield label="Idea description" name="ideaDescrption"
					value="%{idea.ideaDescrption}"></s:textfield>
				<%-- 						<s:submit value="Update" ></s:submit> --%>
<%-- 				<input type="hidden" class="form-control"
					value="<%=session.getAttribute("userid")%>" name="proOwner" />
				<s:hidden name="proID" value="%{idea.ideaId}"></s:hidden> --%>

			</s:form>
	<section id="main" class="column"> <article
		class="module width_full"> <header>
	<h3>List of bids</h3>
	</header>
	<div class="module_content">
		<article class="stats_graph">
		<div class="form-group">
			<br>
		</div>
		<div class="module-body">
			<div class="tab_container">
				<div id="tab1" class="tab_content">
					<table class="tablesorter" cellspacing="0">
						<thead>
							<tr>
								<th align="left"> username</th>
								<th align="left">firstname</th>
								<th>lastname</th>
								<th>country</th>
								<th>Bid Accept</th>
							</tr>
						</thead>

<s:if test="%{taskBids.isEmpty()}">No bids!</s:if>
<s:else>
			<s:iterator value="allBids">
			
			<tr>
					<td>
							<s:property  value="username"/>
					</td>
					<td>
		
							<s:property  value="firstname"/>		
					</td>			
					<td>		
							<s:property  value="lastname"/>
					</td>		
					<td>		
							<s:property  value="country"/>	
					</td>		
					<td>		    			
	    			<s:form action="assignIdea">						
						 <s:hidden name="ideaId" value="%{ideaId}" ></s:hidden>
						 <s:hidden name="ideaFlag" value="2" ></s:hidden>
						 <s:hidden name="ideaPM" value="%{userid}" ></s:hidden>
    				
      				<s:submit></s:submit>
                   </s:form>
                   </td>
                   			</tr>	 
                   
      </s:iterator>
         </s:else>
                        					
</body>
</html>