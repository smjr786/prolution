<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:insertAttribute name="title" ignore="true" />
</title>
</head>

<body>

<table  cellpadding="2" cellspacing="2" width="100%" align="center" >

	<tr>
		<td height="20%" colspan="2"  valign="top" align="left">
   		<tiles:insertAttribute  name="banner" /><br/>
   		<hr/>
   		</td>
 
  </tr>
  
  <tr>   
   		<td height="0" colspan="0"  valign="top" align="left">
   		<tiles:insertAttribute name="body" /><br/>
   		<hr/>
   		</td>
    
   </tr>
   <tr>
   
   <td height="20%" colspan="2">
   <tiles:insertAttribute name="footer" /><br/>
   </td>
   </tr>
   </table>
</body>
</html>