<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="/struts-tags" prefix="s" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Dash board(Contributor)</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="bootstrap/css/layout.css" type="text/css" media="screen" />
        <script src="bootstrap/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    <body>
      
  <header id="header">
		<hgroup>
			<h1 class="site_title">WELCOME &nbsp; ${sessionScope.username}</h1>
			<h2 class="section_title">Contributor </h2>
			<div class="btn_view_site"> 
			  <!--this code to be changed  -->    
			    <!-- <a href="signin.jsp">Log Out</a> -->
				<!-- <s:form action="logout"><a href="">Log Out</a></s:form> -->    
			      <s:url action="logout.action" var="aURL"/>
				<s:form action="logout"><a href="logout">Log Out</a></s:form>  
		   </div>
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
<%-- 					${sessionScope.username}
			<p><s:label label="%{sessionScope.username}"  ></s:label></p> --%>
			
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="pm/dashboardPm.jsp">Contributor</a> <div class="breadcrumb_divider"></div> <a class="current">Dashboard</a></article>
		</div>
	</section>

    
  
   <script src="bootstrap/js/vendor/jquery-1.11.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="bootstrap/js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
        <script src="bootstrap/js/vendor/bootstrap.js"></script>
        <script src="bootstrap/js/main.js"></script>
<body>

</body>
</html>