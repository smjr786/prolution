<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tasks Available</title>
<link rel="stylesheet" href="bootstrap/css/layout.css" type="text/css"
	media="screen" />

</head>
<body>
	<section id="main" class="column"> <article
		class="module width_full"> <header>
	<h3>Available Projects</h3>
	</header>
	<div class="module_content">
		<article class="stats_graph">
		<div class="form-group">
			<label> </label>
		</article>
		<div class="tab_container">
			<div id="tab1" class="tab_content">
				<table class="tablesorter" cellspacing="0">
					<thead>
						<tr>

							<th align="left">Title</th>
							<th>Description</th>
							<th>Domain</th>
							<th>Posted on</th>
							<th>Budget</th>
							<th>Duration</th>
						</tr>
					</thead>
					<s:iterator value="tasks">
						<%-- <s:url id="update" action="taskpost">
		<s:param name="proj.proID" value="taskID"/>
	    <s:param name="taskProId" value="taskID" />
	    </s:url>
	    <s:a href="%{update}">
		</s:a> --%>

						<tr>
							<td><s:property value="taskTitle" /></td>
							<td><s:property value="taskDescrption" /></td>
							<td><s:property value="taskDomain" /></td>
							<td><s:property value="taskPostedOn" /></td>
							<td><s:property value="taskBudget" /></td>
							<td><s:property value="taskDuration" /></td>
							<td><s:form action="bidsTask">
									<input type="hidden"
										value="<%=session.getAttribute("userid")%>" name="bidsUser" />
									<s:hidden type="hidden" value="%{taskID}" name="bidsTask" />
									<input type="hidden" value="1" name="bidsFlag" />
									<s:submit value="bids"></s:submit>
								</s:form></td>
						</tr>
					</s:iterator>
				</table>
			</div>

		</div>
		<div class="clear"></div>
	</div>
	</article> </article> </section>
</body>
</html>