package prolution.actions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import prolution.dao.UserDao;
import prolution.dao.ideaDao;
import prolution.dao.projectDao;
import prolution.dao.taskDao;
import prolution.entity.tb_idea;
import prolution.entity.tb_project;
import prolution.entity.tb_tasks;
import prolution.entity.tb_users;

public class ProjectTasks {
	
	private tb_project proj=new tb_project();

	private tb_idea idea=new tb_idea();
	
	private int proID;
	private String ideaCommit;
	private int ideaId;
	private int rated;
	private int rater;
	private int rateTask;
	private int rateIdea;
	private int rating;
	private ArrayList<tb_tasks> tasks = new ArrayList<tb_tasks>();
	private String currentDate;

	tb_users user=new tb_users();
	
	UserDao daouser=new UserDao();
	
	
	
	
	public int getRated() {
		return rated;
	}

	public void setRated(int rated) {
		this.rated = rated;
	}

	public int getRater() {
		return rater;
	}

	public void setRater(int rater) {
		this.rater = rater;
	}

	public int getRateTask() {
		return rateTask;
	}

	public void setRateTask(int rateTask) {
		this.rateTask = rateTask;
	}

	public int getRateIdea() {
		return rateIdea;
	}

	public void setRateIdea(int rateIdea) {
		this.rateIdea = rateIdea;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public tb_users getUser() {
		return user;
	}

	public void setUser(tb_users user) {
		this.user = user;
	}

	public String getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}

	taskDao daoTask=new taskDao();

	

	public String getIdeaCommit() {
		return ideaCommit;
	}

	public void setIdeaCommit(String ideaCommit) {
		this.ideaCommit = ideaCommit;
	}

	public int getIdeaId() {
		return ideaId;
	}

	public void setIdeaId(int ideaId) {
		this.ideaId = ideaId;
	}

	public ArrayList<tb_tasks> getTasks() {
		return tasks;
	}

	public void setTasks(ArrayList<tb_tasks> tasks) {
		this.tasks = tasks;
	}

	public int getProID() {
		return proID;
	}

	public void setProID(int proID) {
		this.proID = proID;
	}

	public tb_project getProj() {
		return proj;
	}

	public void setProj(tb_project proj) {
		this.proj = proj;
	}

	projectDao projDao=new projectDao();
	
	/*public String execute(){
		this.proj=projDao.aproj(id);
		return "SUCCESS";
	}*/
	
	
	
	public String projectTasks()  throws Exception{
		//System.out.println(student.getRegno());
		/*if((this.proj != null) && (this.proj.getProID() != 0)){
			this.proj = projDao.aproj();
		}*/
		this.proj = projDao.aproj(getProID());
		this.tasks=daoTask.projectTasks(getProID());
		Date dNow =new Date();
		   SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd");
		setCurrentDate(ft.format(dNow));

		return "SUCCESS";
	}
	
	public tb_idea getIdea() {
		return idea;
	}

	public void setIdea(tb_idea idea) {
		this.idea = idea;
	}

	ideaDao daoIdea=new ideaDao();
	
	public String ideaTasks()  throws Exception{
		//System.out.println(student.getRegno());
		/*if((this.proj != null) && (this.proj.getProID() != 0)){
			this.proj = projDao.aproj();
		}*/
		this.idea = daoIdea.anIdea(getIdeaId());
		this.user=daouser.projectOwner(this.idea.getIdeaOwner());
		this.tasks=daoTask.ideaTasks(getIdeaId());
		Date dNow =new Date();
		   SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd");
		setCurrentDate(ft.format(dNow));

		return "SUCCESS";
	}
	
	public String rateEn(){		
		this.idea = daoIdea.anIdea(getIdeaId());
		daouser.rateUser(getRating(), getIdeaId(), getRateTask(), this.idea.getIdeaOwner(), getRater());
		this.tasks=daoTask.ideaTasks(getIdeaId());
		this.user=daouser.projectOwner(this.idea.getIdeaOwner());
		Date dNow =new Date();
		   SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd");
		setCurrentDate(ft.format(dNow));
		return "input";
}
	
	public String p2eMsg(){		
		daoIdea.e2pMsg(getIdeaId(), getIdeaCommit());
		this.idea = daoIdea.anIdea(getIdeaId());
		this.tasks=daoTask.ideaTasks(getIdeaId());
		this.user=daouser.projectOwner(this.idea.getIdeaOwner());
		Date dNow =new Date();
		   SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd");
		setCurrentDate(ft.format(dNow));
		return "input";
}

}
