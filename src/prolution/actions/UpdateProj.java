package prolution.actions;

import java.util.ArrayList;
import java.util.Date;

import prolution.dao.projectDao;
import prolution.entity.tb_project;

public class UpdateProj {
	
	private static final long serialVersionUID = 1L;
	private int proID;
	private int proIdeaId;	
	private String proTitle;	
	private String proDomain;	
	private String proPostedOn;	
	private String proBudget;	
	private String proDuration;	
	private int proFlag;		
	private String proDescrption;
	private ArrayList<tb_project> project = new ArrayList<tb_project>();
    private Integer proOwner;


	
	
	
	projectDao pDao=new projectDao();
	public String updateProj(){
		pDao.updateProj(proID, proIdeaId, proOwner, proTitle, proDescrption, proDomain, proPostedOn, proBudget, proDuration, proFlag);		
		this.project=pDao.myProjects(getProOwner());
		return "success";
	}
	
	

	public int getProFlag() {
		return proFlag;
	}



	public void setProFlag(int proFlag) {
		this.proFlag = proFlag;
	}



	public void setProOwner(int proOwner) {
		this.proOwner = proOwner;
	}
	public int getProID() {
		return proID;
	}
	public void setProID(int proID) {
		this.proID = proID;
	}
	public int getProIdeaId() {
		return proIdeaId;
	}
	public void setProIdeaId(int proIdeaId) {
		this.proIdeaId = proIdeaId;
	}
	public String getProTitle() {
		return proTitle;
	}
	public void setProTitle(String proTitle) {
		this.proTitle = proTitle;
	}
	public String getProDomain() {
		return proDomain;
	}
	public void setProDomain(String proDomain) {
		this.proDomain = proDomain;
	}
	public String getProPostedOn() {
		return proPostedOn;
	}
	public void setProPostedOn(String proPostedOn) {
		this.proPostedOn = proPostedOn;
	}
	public String getProBudget() {
		return proBudget;
	}
	public void setProBudget(String proBudget) {
		this.proBudget = proBudget;
	}
	public String getProDuration() {
		return proDuration;
	}
	public void setProDuration(String proDuration) {
		this.proDuration = proDuration;
	}
	
	public String getProDescrption() {
		return proDescrption;
	}
	public void setProDescrption(String proDescrption) {
		this.proDescrption = proDescrption;
	}
	public ArrayList<tb_project> getProject() {
		return project;
	}
	public void setProject(ArrayList<tb_project> project) {
		this.project = project;
	}
	public Integer getProOwner() {
		return proOwner;
	}
	public void setProOwner(Integer proOwner) {
		this.proOwner = proOwner;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
