package prolution.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "tb_users", catalog = "db_prolution")
public class tb_users {
	
	@Id
	@Column(name = "userid", unique = true, nullable = false)
	private int userid;
	
	@Column(name = "username", nullable = false, length = 80)
	private String username;
	
	@Column(name = "upassword", nullable = false, length = 80)
	private String upassword;
	
	@Column(name = "email", nullable = true, length = 80)
	private String email;
	
	@Column(name = "firstname", nullable = true, length = 80)
	private String firstname;
	
	@Column(name = "lastname", nullable = true, length = 80)
	private String lastname;
	
	@Column(name = "usertype", nullable = true, length = 80)
	private String usertype;
	
	@Column(name = "country", nullable = true, length = 80)
	private String country;

	
	public tb_users() {
		// TODO Auto-generated constructor stub
	}


	public int getUserid() {
		return userid;
	}


	public void setUserid(int userid) {
		this.userid = userid;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getUpassword() {
		return upassword;
	}


	public void setUpassword(String upassword) {
		this.upassword = upassword;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getFirstname() {
		return firstname;
	}


	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}


	public String getLastname() {
		return lastname;
	}


	public void setLastname(String lastname) {
		this.lastname = lastname;
	}


	public String getUsertype() {
		return usertype;
	}


	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public tb_users(int userid, String username, String upassword,
			String email, String firstname, String lastname, String usertype,
			String country) {
		super();
		this.userid = userid;
		this.username = username;
		this.upassword = upassword;
		this.email = email;
		this.firstname = firstname;
		this.lastname = lastname;
		this.usertype = usertype;
		this.country = country;
	}

	


	
	
	


}
