package prolution.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "SearchedTasks", catalog = "db_prolution")
public class SearchedTasks implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "taskID", unique = true, nullable = true)
	private int taskID;
	
	private long noOfBids;
	
	public long getNoOfBids() {
		return noOfBids;
	}

	public void setNoOfBids(long noOfBids) {
		this.noOfBids = noOfBids;
	}

	@Column(name = "taskOwner", unique = true, nullable = true)
	private int taskOwner;
	
	@Column(name = "taskTitle", nullable = false, length = 80)
	private String taskTitle;
	
	@Column(name = "taskDomain", nullable = false, length = 80)
	private String taskDomain;
	
	@Column(name = "taskPostedOn", nullable = false, length = 80)
	private String taskPostedOn;
	
	@Column(name = "taskBudget", nullable = false, length = 80)
	private String taskBudget;
	
	@Column(name = "taskDuration", nullable = false, length = 80)
	private String taskDuration;
	/*
	@Column(name = "taskFlag", nullable = false, length = 2)
	private int taskFlag;
		*/
	@Column(name = "taskDescrption", nullable = true)
	private String taskDescrption;
	
	@Column(name = "taskCommit", nullable = true)
	private String taskCommit;

	public int getTaskID() {
		return taskID;
	}

	public void setTaskID(int taskID) {
		this.taskID = taskID;
	}

	public int getTaskOwner() {
		return taskOwner;
	}

	public void setTaskOwner(int taskOwner) {
		this.taskOwner = taskOwner;
	}

	public String getTaskTitle() {
		return taskTitle;
	}

	public void setTaskTitle(String taskTitle) {
		this.taskTitle = taskTitle;
	}

	public String getTaskDomain() {
		return taskDomain;
	}

	public void setTaskDomain(String taskDomain) {
		this.taskDomain = taskDomain;
	}

	public String getTaskPostedOn() {
		return taskPostedOn;
	}

	public void setTaskPostedOn(String taskPostedOn) {
		this.taskPostedOn = taskPostedOn;
	}

	public String getTaskBudget() {
		return taskBudget;
	}

	public void setTaskBudget(String taskBudget) {
		this.taskBudget = taskBudget;
	}

	public String getTaskDuration() {
		return taskDuration;
	}

	public void setTaskDuration(String taskDuration) {
		this.taskDuration = taskDuration;
	}

	public String getTaskDescrption() {
		return taskDescrption;
	}

	public void setTaskDescrption(String taskDescrption) {
		this.taskDescrption = taskDescrption;
	}

	public String getTaskCommit() {
		return taskCommit;
	}

	public void setTaskCommit(String taskCommit) {
		this.taskCommit = taskCommit;
	}


/*
	public int getTaskFlag() {
		return taskFlag;
	}

	public void setTaskFlag(int taskFlag) {
		this.taskFlag = taskFlag;
	}
*/
	public SearchedTasks(int taskID, long noOfBids, int taskOwner,
			String taskTitle, String taskDomain, String taskPostedOn,
			String taskBudget, String taskDuration,  String taskDescrption,
			String taskCommit) {
		super();
		this.taskID = taskID;
		this.noOfBids = noOfBids;
		this.taskOwner = taskOwner;
		this.taskTitle = taskTitle;
		this.taskDomain = taskDomain;
		this.taskPostedOn = taskPostedOn;
		this.taskBudget = taskBudget;
		//this.taskFlag=taskFlag;
		this.taskDuration = taskDuration;
		this.taskDescrption = taskDescrption;
		this.taskCommit = taskCommit;
	}

	public SearchedTasks() {
		// TODO Auto-generated constructor stub
	}
	
	


}
