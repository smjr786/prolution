<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@taglib uri="/struts-tags" prefix="s"%>
            <%@taglib uri="/struts-jquery-grid-tags" prefix="sjg" %>
            <%@taglib uri="/struts-json-tags" prefix="json" %>
            <%@taglib uri="/struts-jquery-tags" prefix="sj" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
  <s:url id="remoteurl" action="listProjects" />
<sjg:grid id="gridtable"
    caption="Students..."
    dataType="json"
    href="%{remoteurl}" 
    pager="true" 
    gridModel="gridModel"
    rowNum="10" 
    navigator="true"
    navigatorAdd="false"
    navigatorDelete="false"
    navigatorEdit="false"
    navigatorRefresh="true"
    navigatorSearch="false"
    navigatorView="false"
    rownumbers="true"
    rowList="10,20,30" 
    viewrecords="true"
    autowidth="true"
    >
    <sjg:gridColumn name="ideaID" hidden="true" search="false"
        key="true" index="ideaID" title="ideaID" sortable="false" />
    <sjg:gridColumn name="ideaTitle" align="left"
        index="ideaTitle" title="Student Name" sortable="false"/>
    <sjg:gridColumn name="ideaDescrption" align="left"
        index="ideaDescrption" title="Student Address" sortable="false"/>
</sjg:grid> 
</body>
</html>