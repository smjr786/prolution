package prolution.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "availabletasks", catalog = "db_prolution")
public class availabletasks implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6431649912954203362L;



	/**
	 * 
	 */



	@Id
	@Column(name = "taskID", nullable = true)
	private int taskID;
	


	private String taskTitle;
	
	private String taskDomain;
	
	private String taskPostedOn;
	
	private String taskBudget;
	
	private String taskDuration;

	
	private String taskDescrption;




	public availabletasks() {
		// TODO Auto-generated constructor stub
	}



















	public int getTaskID() {
		return taskID;
	}



















	public void setTaskID(int taskID) {
		this.taskID = taskID;
	}



















	public String getTaskTitle() {
		return taskTitle;
	}




	public void setTaskTitle(String taskTitle) {
		this.taskTitle = taskTitle;
	}




	public String getTaskDomain() {
		return taskDomain;
	}




	public void setTaskDomain(String taskDomain) {
		this.taskDomain = taskDomain;
	}




	public String getTaskPostedOn() {
		return taskPostedOn;
	}




	public void setTaskPostedOn(String taskPostedOn) {
		this.taskPostedOn = taskPostedOn;
	}




	public String getTaskBudget() {
		return taskBudget;
	}




	public void setTaskBudget(String taskBudget) {
		this.taskBudget = taskBudget;
	}




	public String getTaskDuration() {
		return taskDuration;
	}




	public void setTaskDuration(String taskDuration) {
		this.taskDuration = taskDuration;
	}




	public String getTaskDescrption() {
		return taskDescrption;
	}




	public void setTaskDescrption(String taskDescrption) {
		this.taskDescrption = taskDescrption;
	}




	public availabletasks(int taskID,  String taskTitle,
			String taskDomain, String taskPostedOn, String taskBudget,
			String taskDuration, String taskDescrption) {
		super();
		this.taskID = taskID;
		this.taskTitle = taskTitle;
		this.taskDomain = taskDomain;
		this.taskPostedOn = taskPostedOn;
		this.taskBudget = taskBudget;
		this.taskDuration = taskDuration;
		this.taskDescrption = taskDescrption;
	}
	
	
	

	

}
