package prolution.dao;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import prolution.entity.tb_project;
import prolution.entity.tb_rate;
import prolution.entity.tb_tasks;
import prolution.entity.tb_users;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class UserDao {
	
	@SuppressWarnings("unchecked")
    public boolean find(String username, String password) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = " from tb_users u where u.username='" + username + "' and u.upassword='" + password + "'";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        List<tb_users> list = query.list();
        if (list.size() > 0) {
            session.close();
            return true;
        }
        session.close();
        return false;
    }
	
    public String rtype(String name) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = " select u.usertype from tb_users u where u.username='" + name + "'";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        String result=(String) query.uniqueResult();
        
        session.close();
        return result;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public boolean regis(String username, String upassword, String firstname,String lastname,String email,String usertype,String country) {
		Session session = null;
		Transaction txn = null;
		boolean var=false;
		try {
			@SuppressWarnings("deprecation")
			SessionFactory sessionFactory = 
			        new Configuration().configure().buildSessionFactory();
			    session = sessionFactory.openSession(); 
			    txn = session.beginTransaction();
        String SQL_QUERY = " from tb_users u where u.username='" + username + "'";
        Query query = session.createQuery(SQL_QUERY);
        List<tb_users> list = query.list();
        if (list.size() == 0){
        	
        tb_users user=new tb_users();
        user.setEmail(email);
        user.setFirstname(firstname);
        user.setLastname(lastname);
        user.setUpassword(upassword);
        user.setUsertype(usertype);
        user.setUsername(username);
        user.setCountry(country);
        
        
        session.save(user);
        txn.commit();
        var=true;}
        

        } catch (Exception e) { 
            System.out.println(e.getMessage());
        } finally {
            if (!txn.wasCommitted()) {
                txn.rollback();
            }

            session.flush();  
            session.close(); 
        }
        
    	return var;
        }
	
    public int getUserId(String username) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = " select u.userid from tb_users u where u.username='" + username + "'";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        int result=(int) query.uniqueResult();
        
        session.close();
        return result;
	}
	
	
	
	 @SuppressWarnings("unchecked")
    public ArrayList<tb_users> searchedUsers( ) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from tb_users";

        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        ArrayList<tb_users> objcoll=new ArrayList<>();
        List<tb_users> list = query.list();
        for (int i =0; i<list.size(); i++) {
        	tb_users user=new tb_users();
        	
        	user.setCountry(list.get(i).getCountry());
        	user.setEmail(list.get(i).getEmail());
        	user.setFirstname(list.get(i).getFirstname());
        	user.setLastname(list.get(i).getLastname());
        	user.setUserid(list.get(i).getUserid());
        	user.setUsername(list.get(i).getUsername());
        	user.setUsertype(list.get(i).getUsertype());
        
          
        	objcoll.add(user);
        }       
        session.close();
        return objcoll;
    } 
    
    
	 @SuppressWarnings("unchecked")
	    public double userRanking(int id) {
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        //session.beginTransaction();
	        String SQL_QUERY = "select avg(r.rating) from tb_rate r WHERE r.rated='" + id + "'";

	        System.out.println(SQL_QUERY);
	        Query query = session.createQuery(SQL_QUERY);  	        
	        
	        Double result=(Double)query.uniqueResult();
	        if (result==null){
	        	result=(double) 0;	        	
	        }
	       // result=(int)query.uniqueResult();	          	           
	        session.close();
	        return result;
	    } 
	 
		@SuppressWarnings("unchecked")
		public void rateUser(
				  int rating,
				  int rateIdea,
				  int rateTask,
				  int rated,
				  int rater ) {
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        session.beginTransaction();
	        tb_rate rate=new tb_rate();
rate.setRated(rated);
rate.setRateIdea(rateIdea);
rate.setRater(rater);
rate.setRateTask(rateTask);
rate.setRating(rating);
	   
		 session.save(rate);
	        session.getTransaction().commit();
	        session.close();
		}
		
	    public tb_users taskOwner(int id) {
	    	tb_users user=new tb_users(); 
	    	try {
	    	Session session = HibernateUtil.getSessionFactory().openSession();
	    	user=(tb_users)session.get(tb_users.class, id);
	       session.close();
	    	 } catch (Exception e) {      

	    }
	         return user;

	}
	    
	    public tb_users projectOwner(int id) {
	    	tb_users user=new tb_users(); 
	    	try {
	    	Session session = HibernateUtil.getSessionFactory().openSession();
	    	user=(tb_users)session.get(tb_users.class, id);
	       session.close();
	    	 } catch (Exception e) {      

	    }
	         return user;

	}
		
	
    }


