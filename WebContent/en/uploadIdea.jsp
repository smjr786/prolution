<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
            <%@taglib uri="/struts-tags" prefix="s"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
        <link rel="stylesheet" href="pm/css/jquery-ui.css">

<script src="pm/js/jquery-1.11.2.min.js"></script>
  <script src="pm/js/jquery-ui.js"></script>
<script>
$(function() {
    //$( "#format" ).change(function() {
      $( "#datepicker" ).datepicker( {dateFormat: 'yy-mm-dd'} );
    //});
     // $( "#datepicker" ).datepicker();

  });
  $(function() {
	    $( "#ideaDomain" ).selectmenu();
	 
	    $( "#files" ).selectmenu();
	 
	    $( "#number" )
	      .selectmenu()
	      .selectmenu( "menuWidget" )
	        .addClass( "overflow" );
	  });
	  
  </script>
<style>
    fieldset {
      border: 0;
       width: 200px;
    }
    label {
      display: block;
      margin: 30px 0 0 0;
    }
    select {
      width: 200px;
    }
    .overflow {
      height: 200px;
    }
  </style>
</head>

<body>




		<s:form action="postIdea">
		
						<label> Idea Title </label> 
    					<input type="hidden" class="form-control" value="<%=session.getAttribute("userid")%>" name="ideaOwner" />
	    				<input type="text" class="form-control" name="ideaTitle" placeholder="Enter Project Title">
	    				
	    				
	    				<label> Idea Description </label>
						<textarea name="ideaDescrption" class="form-control" rows="3"></textarea>
						
						<label> Idea Category </label> 					
						
						<fieldset>		
							<select name="ideaDomain" id="ideaDomain" >
      						<option  selected="selected">web</option>
      						<option>Android</option>
      						<option>IOS</option>
      						<option>Desktop</option>
      						<option>other</option>
      						
    						</select>
    					</fieldset>
						
						<label> Idea Budget </label> 
	    				<input type="text" class="form-control" name="ideaBudget" placeholder="Enter Project Budget">
	    				
						<label> Idea Submission </label>
						
						 	<input type="text" class="form-control" name="ideaDuration" placeholder="Enter Project Duration in days">
							
							<!-- <input type="text" id="datepicker" class="form-control" placeholder="Enter Project Duration" name="ideaDuration" size="30"> --> 					
					
						
							
	                    
	                    <label> Idea PostedOn </label>
	    				<!-- <input type="text" class="form-control" name="ideaPostedOn" placeholder="Enter Project Descrption"> -->
	    				<input type="text" id="datepicker" class="form-control" placeholder="Project Posting date" name="ideaPostedOn" size="30">
	    				 		
                            <s:submit></s:submit>
                        					</s:form>

</body>
</html>