<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
                    <%@taglib uri="/struts-tags" prefix="s"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<section id="main" class="column"> <article
		class="module width_full"> <header>
	<h3>List of bids</h3>
	</header>
	
	<div class="module_content">
		<article class="stats_graph">
		<div class="form-group">
			<s:form action="updateProj">
				<s:textfield label="Task Name" value="%{task.taskTitle}"
					name="taskTitle"></s:textfield>
				<s:textfield label="Task Cost" name="taskBudget"
					value="%{task.taskBudget}"></s:textfield>
				<s:textfield label="Task duration" name="taskDuration"
					value="%{task.taskDuration}"></s:textfield>
				<s:textfield label="Task description" name="taskDescrption"
					value="%{task.taskDescrption}"></s:textfield>
				<%-- 						<s:submit value="Update" ></s:submit> --%>
<%-- 				<input type="hidden" class="form-control"
					value="<%=session.getAttribute("userid")%>" name="proOwner" />
				<s:hidden name="proID" value="%{idea.ideaId}"></s:hidden> --%>

			</s:form>
	
	<div class="module_content">
		<article class="stats_graph">
		<div class="form-group">
			<br>
		</div>
		<div class="module-body">
			<div class="tab_container">
				<div id="tab1" class="tab_content">
					<table class="tablesorter" cellspacing="0">
						<thead>
							<tr>
								<th align="left"> username</th>
								<th align="left">firstname</th>
								<th>lastname</th>
								<th>country</th>
								<th>Bid Accept</th>
							</tr>
						</thead>
						<s:if test="%{taskBids.isEmpty()}">No bids!</s:if>
<s:else>
<s:iterator value="taskBids">
<tr>
	<td>
		<s:property value="username"/>
		</td>
			<td>
		
		<s:property value="firstname"/>		
					</td>			
			<td>		
		<s:property value="lastname"/>
				</td>		
			<td>		
		<s:property value="country"/>	
				</td>		
			<td>		
 			<s:form action="assignTask" >
<s:hidden name="taskOwner" value="%{userid}" ></s:hidden>
<s:hidden name="taskID" value="%{taskID}" ></s:hidden>	    			
	  <s:hidden name="taskFlag" value="2" ></s:hidden>    
      <s:submit></s:submit>
                   </s:form>
                   </td>
                   			</tr>	 
                   
      </s:iterator>
      </s:else>
      

</body>
</html>