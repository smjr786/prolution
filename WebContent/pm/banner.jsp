<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@taglib uri="/struts-tags" prefix="s"%>
 

<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8"/>
	<title>Dash board(Project Manager)</title>
	
	<link rel="stylesheet" href="bootstrap/css/layout.css" type="text/css" media="screen" />
	        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="bootstrap/css/templatemo-style.css">
        <link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">

        <script src="bootstrap/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        	<script src="js/jquery.min.js"></script>
<%-- 	<script src="js/jRate.min.js"></script>
	<script type="text/javascript">
		$(function () {
			var that = this;
			//alert("${rating}");
			$("#jRate").jRate({
				rating: "${rating}",
				strokeColor: 'black',
				width: 20,
				height: 20,						
				readOnly:"true" 
				
			});
			
		});
	</script> --%>
	</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title">WELCOME &nbsp; ${sessionScope.username}</h1>
			<h2 class="section_title">Project Manager </h2>
			<div class="btn_view_site"> 
			  <!--this code to be changed  -->    
			    <!-- <a href="signin.jsp">Log Out</a> -->
				<s:form action="logout"><a href="logout">Log Out</a></s:form>  
<!-- 					<div  id="jRate" style="height:10px;width: 40px;"></div> -->
				  
		   </div>
		   
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
				
		
			
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="dashboardPm.jsp">Project Manager</a> <div class="breadcrumb_divider"></div> <a class="current">Dashboard</a></article>
		</div>
	</section><!-- end of secondary bar -->
	
	
	
	
	<!-- end of sidebar -->
	
	
	
	
	


</body>

</html>