package prolution.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "tb_bids", catalog = "db_prolution")
public class tb_bids {
	
	@Id
	@Column(name = "bidsId", unique = true, nullable = true)
	private int bidsId;
	
	@Column(name = "bidsFlag", unique = true, nullable = true)
	private int bidsFlag;
	
	@Column(name = "bidsTask", unique = true, nullable = true)
	private int bidsTask;
	
	@Column(name = "bidsUser", unique = true, nullable = true)
	private int bidsUser;
	
	@Column(name = "bidsIdea", unique = true, nullable = true)
	private int bidsIdea;

	public int getBidsId() {
		return bidsId;
	}

	public void setBidsId(int bidsId) {
		this.bidsId = bidsId;
	}


	public int getBidsFlag() {
		return bidsFlag;
	}

	public void setBidsFlag(int bidsFlag) {
		this.bidsFlag = bidsFlag;
	}

	public int getBidsTask() {
		return bidsTask;
	}

	public void setBidsTask(int bidsTask) {
		this.bidsTask = bidsTask;
	}

	public int getBidsUser() {
		return bidsUser;
	}

	public void setBidsUser(int bidsUser) {
		this.bidsUser = bidsUser;
	}

	public int getBidsIdea() {
		return bidsIdea;
	}

	public void setBidsIdea(int bidsIdea) {
		this.bidsIdea = bidsIdea;
	}

	public tb_bids(int bidsId, int bidsFlag, 
			 int bidsTask, int bidsUser, int bidsIdea) {
		super();
		this.bidsId = bidsId;
		this.bidsFlag = bidsFlag;
		this.bidsTask = bidsTask;
		this.bidsUser = bidsUser;
		this.bidsIdea = bidsIdea;
	}

	public tb_bids() {
		// TODO Auto-generated constructor stub
	}
	
	

}
