<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
    
<!DOCTYPE html>
<html lang="en"> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Pro-Lution - Signup</title>        
      <!--  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"> -->
	   <link rel="stylesheet" type="text/css" href="bootstrap/css/signup.css" />	   	   
</head>
<body>
    <!--BEGIN #signup-form -->
     <div id="signup-form">        
        <!--BEGIN #subscribe-inner -->
            <div>
            <h1 class="headingone">Sign up  </h1><br>
            </div>            
			<s:form action="register"  >            	
                        <p><s:textfield name="firstname" required="true" label="First Name" placeholder="First Name"></s:textfield></p>                
                        <p><s:textfield name="lastname" required="true" label="Last Name" placeholder="Last Name"></s:textfield></p>                               
                        <p><s:textfield name="email" required="true" label="Email" placeholder="Email"></s:textfield></p>                               
                        <p><s:textfield name="username" required="true" label="Username" placeholder="Username"></s:textfield></p>                                              
        	        	<p><s:password name="upassword" required="true" label="Password" placeholder="Password"></s:password></p>
        	            <p><s:textfield name="country" required="true" label="Country" placeholder="Country"></s:textfield></p>                                              
                
                <div>
                <label>Choose how you want to Use </label>
                 <s:radio id="radiobutton" name="usertype" title="label1" label="Signing up as" 
    						list="#{'pm':'Project Manager','cn':'Contributor','en':'Enterpreneur'}"/> 
	            <p>
                <!-- <button id="submit" type="submit">Submit</button> -->
                
                <!-- <button class="myButton" type="submit" > Signup</button> -->
                 <!--<s:form action="register"><s:submit  value="Signup" cssClass="myButton" /></s:form>-->
<%--                  <s:submit value="Signup" cssClass="myButton" ></s:submit>
 --%>                
 <s:submit value="Signup"  cssClass="myButton" ></s:submit> 
                </p>
	           </div>            
</s:form>
<script src="./jquery/jquery-1.11.1.min.js"></script>
<script src="./jquery/jquery.form-validator.min.js"></script>
<script>
  $.validate();
  $('#my-textarea').restrictLength( $('#max-length-element') );   
</script>            
		<div id="required">
		<p>* Required Fields<br/>
		NOTE: You must activate your account after sign up</p>
		</div>
            </div>        
        <!--END #signup-inner -->
     <!--END #signup-form -->       
</body>
</html>