<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
            <%@taglib uri="/struts-tags" prefix="s"%>
            <%@taglib uri="/struts-jquery-grid-tags" prefix="sjg" %>
            <%@taglib uri="/struts-json-tags" prefix="json" %>
            <%@taglib uri="/struts-jquery-tags" prefix="sj" %>
            
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<sj:head jqueryui="true" jquerytheme="redmond" />

</head>
<body>
<s:iterator value="project">
<s:form action="approveRequest">
	    <s:url id="update" action="project">
		<s:param name="proj.proID" value="proID"/>
	    <s:param name="taskProId" value="proID" />
	    </s:url>
	<s:a href="%{update}">
		<s:property value="proTitle"/>, 	
		<s:property value="proBudget"/>, 		
		<s:property value="proDuration"/> 	
		<s:property value="proDescrption"/> 
	</s:a>			 
	<br />
	<s:submit>bids</s:submit>
	</s:form>
</s:iterator>


  <s:url id="remoteurl" action="listProjects" />
<sjg:grid id="gridtable"
    caption="Students..."
    dataType="json"
    href="%{remoteurl}" 
    pager="true" 
    gridModel="project"
    rowNum="10" 
    navigator="true"
    navigatorAdd="false"
    navigatorDelete="false"
    navigatorEdit="false"
    navigatorRefresh="true"
    navigatorSearch="false"
    navigatorView="false"
    rownumbers="true"
    rowList="10,20,30" 
    viewrecords="true"
    autowidth="true"
    >
    <sjg:gridColumn name="object.proTitle" hidden="true" search="false"
        key="true" index="studentID" title="Student ID" sortable="false" />
    <sjg:gridColumn name="object.proBudget" align="left"
        index="studentName" title="Student Name" sortable="false"/>
    <sjg:gridColumn name="object.proDuration" align="left"
        index="studentAddress" title="Student Address" sortable="false"/>
</sjg:grid> 

</body>
</html>