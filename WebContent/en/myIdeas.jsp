<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
 


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>My Ideas</title>
<link rel="stylesheet" href="bootstrap/css/layout.css" type="text/css"
	media="screen" />
</head>
<body>
	<section id="main" class="column"> <article
		class="module width_full"> <header>
	<h3>List of Projects</h3>
	</header>
	<div class="module_content">
		<article class="stats_graph">
		<div class="form-group">
			<br>
		</div>
		
	 
		<div class="module-body">
			<div class="tab_container">
				<div id="tab1" class="tab_content">
				
				
					<table class="tablesorter" cellspacing="0"> 
						<thead>
							<tr>
								<th align="left">Project Name</th>
								<th align="left">Project Description</th>
								<th>Domain</th>
								<th>Posted on</th>
								<th>Budget</th>
								<th>Duration</th>
							</tr>
						</thead>
						
					 
						<s:iterator value="ideas">
							<tr>
								<td><s:property value="ideaTitle" /></td>

								<td><s:property value="ideaDescrption" /></td>

								<td><s:property value="ideaDomain" /></td>

								<td><s:property value="ideaPostedOn" /></td>

								<td><s:property value="ideaBudget" /></td>

								<td><s:property value="ideaDuration" /></td>

								<td><s:form action="myIdea">
										<s:hidden name="ideaId" value="%{ideaId}"></s:hidden>
										<s:submit></s:submit>
									</s:form></td>
							</tr>
						</s:iterator>
					</table>
					
					
					
			
					
					
					
					
					
				</div>
			</div>
		</div>
		</article>
		<div class="clear"></div>
	</div>
	</article><!-- end of stats article -->
	<div class="clear"></div>
	<div class="spacer"></div>
	</section>
</body>
</html>