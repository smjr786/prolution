<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
                <%@taglib uri="/struts-tags" prefix="s"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><p>prolution</p></title>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bootsrtap Free Admin Template - SIMINTA | Admin Dashboad Template</title>
    <!-- Core CSS - Include with every page -->
    <link href="assets/plugins/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/plugins/pace/pace-theme-big-counter.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet" />
      <link href="assets/css/main-style.css" rel="stylesheet" />
        <link rel="stylesheet" href="pm/css/jquery-ui.css">
          <link rel="stylesheet" href="pm/css/style.css">
     
<script src="pm/js/jquery-1.11.2.min.js"></script>
  <script src="pm/js/jquery-ui.js"></script>
<script>
  $(function() {
    //$( "#format" ).change(function() {
      $( "#datepicker" ).datepicker( {dateFormat: 'yy-mm-dd'} );
    //});
     // $( "#datepicker" ).datepicker();

  });

  $(function() {
	    $( "#proDomain" ).selectmenu();
	 
	    $( "#files" ).selectmenu();
	 
	    $( "#number" )
	      .selectmenu()
	      .selectmenu( "menuWidget" )
	        .addClass( "overflow" );
	  });
	  
  </script>
<style>
    fieldset {
      border: 0;
       width: 200px;
    }
    label {
      display: block;
      margin: 30px 0 0 0;
    }
    select {
      width: 200px;
    }
    .overflow {
      height: 200px;
    }
  </style>
</head>

<body>
<%
   response.setHeader( "Pragma", "no-cache" );
   response.setHeader( "Cache-Control", "no-cache" );
   response.setDateHeader( "Expires", 0 );
%>
<section id="main" class="column">		
		<article class="module width_full">
			<header>
			  	 <h3>Upload New Project</h3>
			</header>
						<div class="module_content">
							<article class="stats_graph">
								<div class="form-group">
    							
	    								<!-- <textarea class="form-control" rows="1" cols="60" placeholder="Post Your Idea"></textarea> -->
	    								  <br>			
								</div>

	        					<div class="module-body">
                            
                            <s:form action="projectposted" >            
                            <div class="flot-chart">
                                <div class="form-group">
    					<label> Project Title </label> 
    					<input type="hidden" class="form-control" value="<%=session.getAttribute("userid")%>" name="proOwner" />
	    				<input type="text" class="form-control" name="proTitle" placeholder="Enter Project Title">	    				
	    					    				<input type="hidden" class="form-control" name="proPostedOn" value="${currentDate}"/> 				
					<label> Project Category </label> 					
					<fieldset>		
	<select name="proDomain" id="proDomain" >
      <option >web</option>
      <option>Android</option>
      <option selected="selected">IOS</option>
      <option>Desktop</option>
      <option>other</option>
    </select>
    </fieldset>
					
					<label> Project Budget </label> 
	    				<input type="text" class="form-control" name="proBudget" placeholder="Enter Project Budget">
					<label> Project Submission </label> 
					<input type="text" id="datepicker" class="form-control" placeholder="Enter Project Duration" name="proDuration" size="30"> 					
					<label> Project Description </label> 
					<textarea name="proDescrption" class="form-control" rows="3"></textarea>	
				</div>
		<div class="module-body">
					<p>
                                	<%-- 	<label>Select Category</label>
                                		<select class="input-short">
                                   		<option value="1"> Web Project </option>
                                    		<option value="2"> Desktop Project </option>
                                    		<option value="3"> Android Project </option>
                                    		<option value="4"> IOS Project </option>
                                		</select>
                            			</p> --%>
				       	<%-- 	<p>
                                		<label> Duration </label>
                                		<input type="text" class="input-short" />
                                		<select class="input-short">
                                   		<option value="1"> Months </option>
                                    		<option value="2"> Days </option>
                                    		<option value="3"> Years </option>
                                		</select>
                           	 		</p> --%>
					
					</div>
                            </div>
<%--                             <s:checkbox name="proAproved" label="Check Me for testing" ></s:checkbox> --%>
                            <s:submit></s:submit>
                        					</s:form>
                 				</div>
							</article>
				
						<div class="clear"></div>
						</div>
		</article>
		<div class="clear"></div>
    <!-- end wrapper -->
		<div class="spacer"></div>
	</section>
</body>

</html>